package extremecarpaccio.client.kotlin.controller

import extremecarpaccio.client.kotlin.model.Amount
import extremecarpaccio.client.kotlin.model.FeedbackMessage
import extremecarpaccio.client.kotlin.model.Order
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.server.ResponseStatusException

@RestController
class WebController {

    @RequestMapping(value = ["/order"], method = [RequestMethod.POST])
    fun answerQuote(
        @RequestBody order: Order,
    ): Amount {
        println("Order received: $order")
        if (order.prices.isEmpty()) return Amount(computeAmount(order))

        // Throw a 404 if you don't want to respond to an order, without penalty
        throw ResponseStatusException(HttpStatus.NOT_FOUND, "cannot answer")
    }

    @RequestMapping(value = ["/feedback"], method = [RequestMethod.POST])
    fun logFeedback(
        @RequestBody message: FeedbackMessage,
    ) {
        println("feedback received: " + message.toString())
    }

    @RequestMapping(value = ["/ping"], method = [RequestMethod.GET])
    fun ping(): String {
        println("ping received")
        return "pong"
    }

    fun computeAmount(order: Order): Double {
        return 0.0
    }
}
