package extremecarpaccio.client.kotlin

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ExtremeCarpaccioKotlinClientApplication

fun main(args: Array<String>) {
    runApplication<ExtremeCarpaccioKotlinClientApplication>(*args)
}
