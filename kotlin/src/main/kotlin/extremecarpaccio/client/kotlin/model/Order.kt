package extremecarpaccio.client.kotlin.model

import java.io.Serializable

data class Order(
    val prices: List<Double>,
    val quantities: List<Long>,
    val country: String,
    val reduction: String,
) : Serializable
