package extremecarpaccio.client.kotlin.model

data class FeedbackMessage(
    val type: String,
    val content: String,
)
