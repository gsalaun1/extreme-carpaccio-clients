package extremecarpaccio.client.kotlin.model

data class Amount(val total: Double)
