package extremecarpaccio.client.java.model;

public record Amount(Double total) {
}