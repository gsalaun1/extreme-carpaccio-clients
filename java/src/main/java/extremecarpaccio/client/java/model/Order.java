package extremecarpaccio.client.java.model;

import java.io.Serializable;
import java.util.List;

public record Order(
        List<Double> prices,
        List<Long> quantities,
        String country,
        String reduction
) implements Serializable {

}
