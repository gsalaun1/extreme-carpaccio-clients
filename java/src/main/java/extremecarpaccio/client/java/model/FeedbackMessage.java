package extremecarpaccio.client.java.model;

public record FeedbackMessage(
        String type,
        String content
) {
}
