package extremecarpaccio.client.java;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExtremeCarpaccioJavaClientApplication {
    public static void main(String[] args) {
        SpringApplication.run(ExtremeCarpaccioJavaClientApplication.class, args);
    }
}