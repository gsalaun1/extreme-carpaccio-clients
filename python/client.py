"Extreme Carpaccio Python Client"

import logging
import sys
from typing import List

from fastapi import FastAPI
from pydantic import BaseModel


class OrderPostSchema(BaseModel):
    """Order Post Schema"""

    prices: List[float]
    quantities: List[int]
    reduction: str
    country: str


class OrderResponse(BaseModel):
    """Order Response Schema"""

    total: float


logging.basicConfig(
    level=logging.INFO,
    stream=sys.stdout,
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
)
logger = logging.getLogger(__name__)
app = FastAPI(title="Extreme Carpaccio Client")


@app.post("/order", response_model=OrderResponse)
def handle_order(order: OrderPostSchema) -> OrderResponse:
    """Handle orders request

    Args:
        order (OrderPostSchema): Schema containing prices, quantities, reduction and country

    Returns:
        OrderResponse: Total price of the order
    """

    # TODO YOUR CODE HERE
    logger.info("Received order: %s", order)

    return OrderResponse(total=1000)


@app.post("/feedback")
def handle_feedback(feedback: dict) -> dict:
    """Handle feedback from the server

    Args:
        feedback (dict): Feedback from the server

    Returns:
        dict: Feedback from the server
    """
    logger.info("Received feedback: %s", feedback)

    # TODO HERE YOU RECEIVE WHAT THE SERVER THINKS ABOUT YOUR WORK

    return feedback


@app.post("/ping")
def handle_ping() -> str:
    """Handle ping request

    Returns:
        str: pong
    """
    return "pong"


@app.get("/")
def handle_hello_world() -> str:
    """Handle hello world request

    Returns:
        str: hello world
    """
    return "hello world"
