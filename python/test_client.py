from fastapi.testclient import TestClient

from client import app

client = TestClient(app)


def test_response_to_hello_world():
    response = client.get("/")
    assert response.status_code == 200
    assert response.text == '"hello world"'


def test_response_to_non_existent_path():
    response = client.post("/unknown", data={"answer": "hello"})
    assert response.status_code == 404


def test_response_to_ping():
    response = client.post("/ping")
    assert response.status_code == 200
    assert response.text == '"pong"'


def test_response_to_order():
    response = client.post(
        "/order",
        json={
            "prices": [1.0, 2.0],
            "quantities": [1, 2],
            "reduction": "STANDARD",
            "country": "FR",
        },
    )
    assert response.status_code == 200
    assert response.json() == {"total": 1000}


def test_response_to_feedback():
    response = client.post("/feedback", json={"What does the fox do": "Something"})
    assert response.status_code == 200
    assert response.json() == {"What does the fox do": "Something"}
