# Extreme Carpaccio Python Client

## Pre-requisites

- Python 3.11+

## How to install

```bash
pip install -r requirements.txt
```

## How to run tests

```bash
pytest .
```

## How to run server

```bash
uvicorn client:app --host 0.0.0.0 --port 8000 --reload
```

## How to run server with logs

```bash
uvicorn client:app --host 0.0.0.0 --port 8000 --reload --log-config ./log.yaml
```
